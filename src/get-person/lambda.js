const Persistence = require('../persistence/persistence.js');
const PersistenceMock = require('../persistence/mock_persistence.js');
const Mapper = require('./mapper.js');
const Logger = require('../utils/logger.js');

/**
 * AWS calls this function at Lambda invocation, since we've tied it in our template as the handler.
 *
 * @param {*} event The parameter object coming from consumer.
 * @param {*} context Context provided by AWS for the invocation.
 */
exports.handler = async (event, context) => {
  try {
    const isMock = event.mock ? true : false;
    const tableName = getTableName(isMock);
    const request = Mapper.mapRequest(event, tableName);
    const person = await getPerson(request, isMock);

    return Mapper.mapResponse(person);
  } catch (error) {
    Logger.error('Problem retrieving data!', error);

    return Mapper.mapError(error);
  }
};

function getTableName(isMock) {
  const tableName = isMock ? PersistenceMock.tableName : Persistence.tableName;

  Logger.log('Resolved table name to: %s.', tableName, isMock);

  return tableName;
}

async function getPerson(request, isMock) {
  const responsePromise = isMock ? PersistenceMock.getPerson(request) : Persistence.getPerson(request);
  const response = await responsePromise;

  Logger.log('Resolved response to: %s.', response, isMock);

  return response;
}
