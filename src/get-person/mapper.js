const Logger = require('../utils/logger');
const NotFoundError = require('../errors/not_found_error');
const MappingError = require('../errors/mapping_error');

exports.mapRequest = (event, tableName) => {
  if (!event || !event.id) {
    Logger.error('No data to map!');
    throw new MappingError();
  }

  const request = {
    Key: {
      UserId: {
        S: event.id,
      },
    },
    TableName: tableName,
  };

  Logger.log('Mapped request:\n%s', request);

  return request;
};

exports.mapResponse = (data) => {
  if (!data || !data.Item) {
    Logger.error('No data to map!');
    throw new MappingError();
  }

  const result = {
    userId: data.Item.UserId.S,
    name: data.Item.name.S,
    age: +data.Item.age.N,
    height: +data.Item.height.N,
    income: +data.Item.income.N,
  };

  Logger.log('Mapped response to: %s.', result);

  return result;
};

exports.mapError = (error) => {
  let result = {
    error: 'TECHNICAL',
    description: "Something went wrong, and couldn't complete request.",
  };

  if (error instanceof NotFoundError) {
    result.error = error.code;
    result.description = "Couldn't find a person with given parameters.";
  }

  return result;
};
