exports.log = (message, value, isMock) => {
  const formattedValue = isObject(value) ? JSON.stringify(value, null, 2) : value;

  if (value !== undefined) {
    if (isMock !== undefined) {
      console.log(`${message}\nUsing mock? %s.`, formattedValue, isMock ? 'Yes' : 'No');
    } else {
      console.log(message, formattedValue);
    }
  } else {
    console.log(message);
  }
};

exports.warn = console.warn;
exports.error = console.error;

function isObject(value) {
  return value !== undefined && value !== null && value.constructor == Object;
}
