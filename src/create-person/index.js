const AWS = require('aws-sdk');
const REGION_NAME = process.env.AWS_REGION;
const DB = new AWS.DynamoDB({
  region: REGION_NAME,
  apiVersion: '2012-08-10',
});
const TABLE_NAME = process.env.tableName;

function mapRequest(event) {
  const params = {
    Item: {
      UserId: {
        S: event.userId,
      },
      name: {
        S: event.name,
      },
      age: {
        N: event.age,
      },
      height: {
        N: event.height,
      },
      income: {
        N: event.income,
      },
    },
    TableName: TABLE_NAME,
  };
  console.log('Params for DB: %s.', JSON.stringify(params, null, 2));

  return params;
}

exports.handler = async (event, context) => {
  console.log('Event: ' + event);
  let params = mapRequest(event);
  let putItemPromise = DB.putItem(params).promise();

  try {
    await putItemPromise;

    console.log('Item saved to DB succesfully!');

    return params.Item;
  } catch (err) {
    console.warn('Problem saving item to database!');
    console.error(err);

    return err;
  }
};
