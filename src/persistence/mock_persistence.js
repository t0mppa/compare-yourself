const TABLE_NAME = 'mock_table';

exports.tableName = TABLE_NAME;

exports.getPerson = async (request) => {
  const response = {
    Item: {
      UserId: { S: request.Key.UserId.S },
      name: { S: 'James' },
      age: { N: '45' },
      height: { N: '180' },
      income: { N: '3500' },
    }
  };

  return response;
};
