const AWS = require('aws-sdk');
const Logger = require('../utils/logger');
const NotFoundError = require('../errors/not_found_error');

const REGION_NAME = process.env.AWS_REGION;
const TABLE_NAME = process.env.tableName;

exports.tableName = TABLE_NAME;

exports.getPerson = async (request) => {
  // Creating the DynamoDB instance here, instead of at class level, makes mocking it far easier.
  const DB = new AWS.DynamoDB({
    region: REGION_NAME,
    apiVersion: '2012-08-10',
  });

  const getItemPromise = DB.getItem(request).promise();

  try {
    let data = await getItemPromise;

    if (!data || !data.Item) {
      Logger.warn('Nothing found!');

      throw new NotFoundError();
    }

    return data;
  } catch (err) {
    Logger.error('Search failed!');
    throw err;
  }
};
