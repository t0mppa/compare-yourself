class MappingError extends Error {
  constructor(...params) {
    super(...params);
  }
}

module.exports = MappingError;
