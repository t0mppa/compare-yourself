class NotFoundError extends Error {
  constructor(code = 'NOT_FOUND', ...params) {
    super(...params);

    this.code = code;
  }
}

module.exports = NotFoundError;
