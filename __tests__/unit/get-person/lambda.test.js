// Silence console for these tests.
global.console = {
  log: jest.fn(),
  warn: jest.fn(),
  error: jest.fn(),
};

const lambda = require('../../../src/get-person/lambda');

const MOCK_JAMES_EVENT = require('../../../events/get-person/MockJames.json');
const JOHN_DOE_EVENT = require('../../../events/get-person/JohnDoe.json');
const BAD_EVENT = require('../../../events/get-person/BadEvent.json');
const NOT_FOUND_EVENT = require('../../../events/get-person/NotFound.json');
const NotFoundError = require('../../../src/errors/not_found_error');
const MappingError = require('../../../src/errors/mapping_error');

describe('Calls for mocked data', () => {
  it('should handle mock data call', async () => {
    // Arrange
    const expected = {
      age: 45,
      height: 180,
      income: 3500,
      name: 'James',
      userId: 'xyz',
    };

    // Act
    const result = await lambda.handler(MOCK_JAMES_EVENT, null);

    // Assert
    expect(result).toEqual(expected);
  });
});

describe('Calls for real data', () => {
  // Tell Jest not to mock the whole class.
  jest.unmock('../../../src/persistence/persistence.js');
  // Then require it, so we can play with it.
  const Persistence = require('../../../src/persistence/persistence.js');

  // Make sure state of mock set in a single test doesn't affect others.
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should handle real data call', async () => {
    // Arrange
    const expected = {
      age: 30,
      height: 172,
      income: 2130,
      name: 'John Doe',
      userId: 'user_123',
    };
    Persistence.getPerson = jest.fn().mockResolvedValue(unmapData(expected));

    // Act
    const result = await lambda.handler(JOHN_DOE_EVENT, null);

    // Assert
    expect(result).toEqual(expected);
  });

  it('should handle real data call that ends in error', async () => {
    // Arrange
    const error = new Error('boo!');
    Persistence.getPerson = jest.fn().mockRejectedValue(error);

    // Act
    const result = await lambda.handler(JOHN_DOE_EVENT, null);

    // Assert
    expect(result).toEqual(errorResponse());
    expect(console.error).toHaveBeenCalledTimes(1);
    expect(console.error.mock.calls[0]).toEqual(['Problem retrieving data!', error]);
  });

  it('should handle real data call with bad event', async () => {
    // Arrange
    const error = new MappingError();

    // Act
    const result = await lambda.handler(BAD_EVENT, null);

    // Assert
    expect(result).toEqual(errorResponse());
    expect(console.error).toHaveBeenCalledTimes(2);
    expect(console.error.mock.calls[0]).toEqual(['No data to map!']);
    expect(console.error.mock.calls[1]).toEqual(['Problem retrieving data!', error]);
  });

  it('should handle real data call that returns not found', async () => {
    // Arrange
    const error = new NotFoundError();
    Persistence.getPerson = jest.fn().mockRejectedValue(error);

    // Act
    const result = await lambda.handler(NOT_FOUND_EVENT, null);

    // Assert
    expect(result).toEqual(errorResponse('notFound'));
    expect(console.error).toHaveBeenCalledTimes(1);
    expect(console.error.mock.calls[0]).toEqual(['Problem retrieving data!', error]);
  });
});

const errorResponse = (type = 'default') =>
  ({
    notFound: {
      error: 'NOT_FOUND',
      description: "Couldn't find a person with given parameters.",
    },
    default: {
      error: 'TECHNICAL',
      description: "Something went wrong, and couldn't complete request.",
    },
  }[type]);

/**
 * Since lambda will use Mapper to map the data in a nice outcome, let's reverse that, so we can mock
 * only Persistence.
 *
 * @param {*} mappedData outcome of the mapping operation
 */
function unmapData(mappedData) {
  return {
    Item: {
      UserId: {
        S: mappedData.userId,
      },
      name: {
        S: mappedData.name,
      },
      age: {
        N: mappedData.age,
      },
      height: {
        N: mappedData.height,
      },
      income: {
        N: mappedData.income,
      },
    },
  };
}
