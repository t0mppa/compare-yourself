// Silence console for these tests.
global.console = {
  log: jest.fn(),
  warn: jest.fn(),
  error: jest.fn(),
};

const mapper = require('../../../src/get-person/mapper.js');
const MappingError = require('../../../src/errors/mapping_error.js');

describe('Testing requests', () => {
  it('should map proper request correctly', () => {
    const request = {
      id: 'user_123',
    };
    const tableName = 'table_a';

    const mappedRequest = mapper.mapRequest(request, tableName);

    expect(mappedRequest.Key.UserId.S).toEqual('user_123');
    expect(mappedRequest.TableName).toBe('table_a');
  });

  it('should throw error when request is empty', () => {
    expect(() => {
      mapper.mapRequest(null);
    }).toThrow(MappingError);
  });

  it('should throw error when request id is missing', () => {
    expect(() => {
      mapper.mapRequest({ foo: 'bar' });
    }).toThrow(MappingError);
  });
});

describe('Testing responses', () => {
  it('should map full response correctly', () => {
    const response = {
      Item: {
        UserId: {
          S: 'user_123',
        },
        name: {
          S: 'Old man',
        },
        age: {
          N: '80',
        },
        height: {
          N: '145',
        },
        income: {
          N: '500',
        },
      },
    };
    const expected = {
      userId: 'user_123',
      name: 'Old man',
      age: 80,
      height: 145,
      income: 500,
    };

    const mappedResponse = mapper.mapResponse(response);

    expect(mappedResponse).toEqual(expected);
  });

  it('should throw error when response is empty', () => {
    expect(() => {
      mapper.mapResponse(null);
    }).toThrow(MappingError);
  });

  it('should throw error when proper response data is missing', () => {
    expect(() => {
      mapper.mapResponse({ foo: 'bar' });
    }).toThrow(MappingError);
  });
});
