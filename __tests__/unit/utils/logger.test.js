global.console = {
  log: jest.fn(),
  warn: jest.fn(),
  error: jest.fn(),
};

const Logger = require('../../../src/utils/logger');
const expectConsole = {
  log(times, params) {
    expect(console.log).toHaveBeenCalledTimes(times);

    const callParams = console.log.mock.calls[0];
    expectParams(params, callParams);
  },
  warn(times, params) {
    expect(console.warn).toHaveBeenCalledTimes(times);

    const callParams = console.warn.mock.calls[0];
    expectParams(params, callParams);
  },
  error(times, params) {
    expect(console.error).toHaveBeenCalledTimes(times);

    const callParams = console.error.mock.calls[0];
    expectParams(params, callParams);
  },
};
const addMockStatus = (message) => message + '\nUsing mock? %s.';
const booleanConversion = (boolean) => (boolean ? 'Yes' : 'No');

describe('Logging', () => {
  const error = {
    name: 'errorName',
    message: 'errorMessage',
  };

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('Check console.log() output', () => {
    const casualMessage = 'Just logging here!';
    const messageWithFormatting = 'Error is %s.';

    it('should be able to log messages from normal console', () => {
      console.log(casualMessage);

      expectConsole.log(1, [casualMessage]);
    });

    it('should be able to log messages from Logger', () => {
      Logger.log(casualMessage);

      expectConsole.log(1, [casualMessage]);
    });

    it('should format JS objects in Logger.log calls', () => {
      const formattedError = JSON.stringify(error, null, 2);
      Logger.log(messageWithFormatting, error);

      expectConsole.log(1, [messageWithFormatting, formattedError]);
    });

    it('should format JS objects and show mock status as true in Logger.log calls', () => {
      const formattedError = JSON.stringify(error, null, 2);
      const messageWithFormattingAndMockStatus = addMockStatus(messageWithFormatting);
      const isMock = true;
      Logger.log(messageWithFormatting, error, isMock);

      expectConsole.log(1, [messageWithFormattingAndMockStatus, formattedError, booleanConversion(isMock)]);
    });

    it('should format JS objects and show mock status as false in Logger.log calls', () => {
      const formattedError = JSON.stringify(error, null, 2);
      const messageWithFormattingAndMockStatus = addMockStatus(messageWithFormatting);
      const isMock = false;
      Logger.log(messageWithFormatting, error, isMock);

      expectConsole.log(1, [messageWithFormattingAndMockStatus, formattedError, booleanConversion(isMock)]);
    });
  });

  describe('Check console.warn() output', () => {
    const warning = 'This is a warning!';

    it('should be able to log warning messages from normal console', () => {
      console.warn(warning);

      expectConsole.warn(1, [warning]);
    });

    it('should be able to log warning messages from Logger', () => {
      Logger.warn(warning);

      expectConsole.warn(1, [warning]);
    });
  });

  describe('Check console.error() output', () => {
    it('should be able to log error messages from normal console', () => {
      console.error(error);

      expectConsole.error(1, [error]);
    });

    it('should be able to log error messages from Logger', () => {
      Logger.error(error);

      expectConsole.error(1, [error]);
    });
  });
});

function expectParams(params, callParams) {
  params.forEach((param, index) => {
    expect(callParams[index]).toEqual(param);
  });

  expect(callParams.length).toBe(params.length);
}
