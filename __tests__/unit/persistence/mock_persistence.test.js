const mock_persistence = require('../../../src/persistence/mock_persistence.js');

it('should return mocked person data', async () => {
  const request = {
    Key : {
      UserId : {
        S: 'user_123'
      }
    }
  };
  const person = await mock_persistence.getPerson(request);

  expect(person.Item.UserId.S).toBe('user_123');
});

it('should return mocked table name', () => {
  expect(mock_persistence.tableName).toBe("mock_table");
});
