global.console = {
  log: jest.fn(),
  warn: jest.fn(),
  error: jest.fn(),
};

const AWS = require('aws-sdk');
const AWSMock = require('aws-sdk-mock');
const persistence = require('../../../src/persistence/persistence.js');
const NotFoundError = require('../../../src/errors/not_found_error.js');

describe('With AWS SDK mocked', () => {
  const emptyRequest = { Key: {}, TableName: '' };

  beforeEach(() => {
    jest.clearAllMocks();
    AWSMock.setSDKInstance(AWS);
  });
  afterEach(() => {
    AWSMock.restore();
  });

  it('should mock getItem from DynamoDB', async () => {
    AWSMock.mock('DynamoDB', 'getItem', (error, callback) => {
      callback(null, { pk: 'foo', sk: 'bar' });
    });

    const DB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });
    const input = { TableName: '', Key: {} };
    expect(await DB.getItem(input).promise()).toStrictEqual({ pk: 'foo', sk: 'bar' });
  });

  it('should handle correct person data', async () => {
    const response = {
      Item: {
        UserId: {
          S: 'user_123',
        },
      },
    };

    AWSMock.mock('DynamoDB', 'getItem', (error, callback) => {
      callback(null, response);
    });

    const person = await persistence.getPerson(emptyRequest);

    expect(person.Item.UserId.S).toBe('user_123');
  });

  it('should handle empty Item data', async () => {
    AWSMock.mock('DynamoDB', 'getItem', (error, callback) => {
      callback(null, { Item: undefined });
    });

    await expect(persistence.getPerson(emptyRequest)).rejects.toEqual(new NotFoundError());
    expect(console.warn).toHaveBeenCalledTimes(1);
    expect(console.warn.mock.calls[0]).toEqual(['Nothing found!']);
  });

  it('should handle empty data', async () => {
    AWSMock.mock('DynamoDB', 'getItem', (error, callback) => {
      callback(null, undefined);
    });

    await expect(persistence.getPerson(emptyRequest)).rejects.toEqual(new NotFoundError());
    expect(console.warn).toHaveBeenCalledTimes(1);
    expect(console.warn.mock.calls[0]).toEqual(['Nothing found!']);
  });

  it('should handle search errors', async () => {
    AWSMock.mock('DynamoDB', 'getItem', (error, callback) => {
      callback('error!', null);
    });

    try {
      const person = await persistence.getPerson(emptyRequest);
    } catch (error) {
      expect(error).toBe('error!');
    }

    expect(console.error).toHaveBeenCalledTimes(1);
    expect(console.error.mock.calls[0]).toEqual(['Search failed!']);
  });
});

it('should return correct table name', () => {
  expect(persistence.tableName).toBe('comparisons');
});
