# create an s3 bucket
aws s3 mb s3://comparisons-sam --profile personal --region eu-central-1 

# package cloudformation, could also use shorthand 'sam package [...]'
aws cloudformation package --s3-bucket comparisons-sam --template-file template.yaml --output-template-file gen/template-generated.yaml --profile personal --region eu-central-1

# deploy
aws cloudformation deploy --template-file gen/template-generated.yaml --stack-name comparisons-sam --capabilities CAPABILITY_IAM --profile personal --region eu-central-1

# test get-person with mock data
aws lambda invoke --function-name "comparisonsGetPerson" --log-type Tail --payload file://./test/get-person/MockJames.json --cli-binary-format raw-in-base64-out --profile personal --region eu-central-1 ./invokes/get-person/mockTestResponse.json